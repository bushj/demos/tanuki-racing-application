// refactor these into a single showEmployeeList function; add comments explaining any changes made
function showDeveloperList(developers) {
    developers.forEach(developer => {
        const expectedSalary = developer.calculateExpectedSalary();
        const experience = developer.getExperience();
        const gitlabLink = developer.getGitlabLink();
        const data = {
            expectedSalary,
            experience,
            gitlabLink
        };

        render(data);
    });
}

function showManagerList(managers) {
    managers.forEach(manager => {
        const expectedSalary = manager.calculateExpectedSalary();
        const experience = manager.getExperience();
        const portfolio = manager.getMBAProjects();
        const data = {
            expectedSalary,
            experience,
            portfolio
        };

        render(data);
    });
}
